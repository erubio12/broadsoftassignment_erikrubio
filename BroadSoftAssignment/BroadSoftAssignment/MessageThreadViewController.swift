//
//  MessageThreadViewController.swift
//  BroadSoftAssignment
//
//  Created by Erik Rubio on 6/12/17.
//  Copyright © 2017 Erik Rubio. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import Photos
import MobileCoreServices


// Creating Rudimentary Users
struct User {
    let id: String
    let name: String
}


class MessageThreadViewController: JSQMessagesViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    let user1 = User(id: "1", name: "Steve")
    let user2 = User(id: "2", name: "Tim")
    
    
    var currentUser: User {
        return user1
    }
    
    // all messages of users1, users2
    var messages = [JSQMessage]()
}




// Extensions for the JSQMessages CocoaPod
extension MessageThreadViewController {
    
    // Handling when the send button is pressed
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        let dateString = dateFormatter.string(from: Date())
        
        let message = JSQMessage(senderId: senderId, displayName: dateString, text: text)
        
        messages.append(message!)
         JSQSystemSoundPlayer.jsq_playMessageSentSound()
        finishSendingMessage()
    }
    
    // Handling Image Picker
    private func handleCameraControl(type: UIImagePickerControllerSourceType) {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        picker.sourceType = type
        picker.mediaTypes = [kUTTypeImage as String]
        present(picker, animated: true, completion: nil)
    }
    
    // When the ImageClip Button is pressed function
    override func didPressAccessoryButton(_ sender: UIButton!) {
        
        let actionSheet = UIAlertController(title: "Media", message: "Select your option", preferredStyle: .actionSheet)
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            actionSheet.addAction(UIAlertAction(title: "Open Camera", style: .default, handler: { action in
                self.handleCameraControl(type: .camera)
            }))
        }
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            actionSheet.addAction(UIAlertAction(title: "Open Library", style: .default, handler: { action in
                self.handleCameraControl(type: .photoLibrary)
            }))
        }
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            actionSheet.dismiss(animated: true, completion: nil)
        }))
        present(actionSheet, animated: true, completion: nil)
        
      
    }
    
    //Adding the TimeStamp to the top of the Message Bubble
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        let message = messages[indexPath.row]
        let messageUsername = message.senderDisplayName
        
        return NSAttributedString(string: messageUsername!)
    }
    

    // Height for TextBubble
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        return 15
    }
    // If ImageAvatar would be created
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let bubbleFactory = JSQMessagesBubbleImageFactory()
        
        let message = messages[indexPath.row]
        
        if currentUser.id == message.senderId {
            return bubbleFactory?.outgoingMessagesBubbleImage(with: UIColor.blue)
        } else {
            return bubbleFactory?.incomingMessagesBubbleImage(with: UIColor.lightGray)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.row]
    }
}



extension MessageThreadViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        // tell JSQMessagesViewController
        // who is the current user
        self.senderId = currentUser.id
        self.senderDisplayName = currentUser.name
        
        self.title = "Tim"
   
            self.messages = getMessages()
        
        
    }
}



extension MessageThreadViewController {
    func getMessages() -> [JSQMessage] {
        

        var messages = [JSQMessage]()
     
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        let dateString = dateFormatter.string(from: Date())
        
        let message1 = JSQMessage(senderId: "1", displayName: dateString, text: "Hey Tim how are you?")
        let message2 = JSQMessage(senderId: "2", displayName: dateString, text: "Fine thanks, and you?")
        
        
        messages.append(message1!)
        messages.append(message2!)
 
        
        return messages
    }
}


