//
//  VibrantTextField.swift
//  BroadSoftAssignment
//
//  Created by Erik Rubio on 6/13/17.
//  Copyright © 2017 Erik Rubio. All rights reserved.
//

import UIKit

class VibrantTextField: UITextField {
    
    
    func shake () {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.05
        animation.repeatCount = 5
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 4, y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 4, y: self.center.y))
        
        self.layer.add(animation, forKey: "position")
        
    }
    
}
