//
//  LoginViewController.swift
//  BroadSoftAssignment
//
//  Created by Erik Rubio on 6/12/17.
//  Copyright © 2017 Erik Rubio. All rights reserved.
//

import UIKit




class LoginViewController: UIViewController, UITextFieldDelegate {
    
 
    // Create Variables
    var button = UIButton()
    var idText = VibrantTextField()
    var passwordText = VibrantTextField()
    var imageView = UIImageView()
    var backgroundImageview = UIImageView()
    var whiteView = UIView()

    var creditsText = UILabel()
    var creditsText2 = UILabel()
    var creditsText3 = UILabel()

    

    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    // All UI Programmatically created and set in function
    func setUI() {


        // ImageView properties
        imageView = UIImageView(frame: CGRect(x: view.frame.width * 0.42, y: view.frame.height * 0.18, width: 50, height: 45))
        imageView.image = #imageLiteral(resourceName: "broadsoftlogo")
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = UIColor.white
        imageView.layer.shadowColor = UIColor.black.cgColor
        imageView.layer.shadowOffset = CGSize.zero
        imageView.layer.shadowOpacity = 0.5
        imageView.layer.shadowRadius = 20
        
        
   
        
        backgroundImageview = UIImageView(frame: CGRect(x: view.frame.width * 0, y: view.frame.height * 0.15, width: view.frame.width, height: 100))
        
        backgroundImageview.image = #imageLiteral(resourceName: "broadImage")
        backgroundImageview.contentMode = .scaleAspectFill
        
        whiteView = UIView(frame: CGRect(x: view.frame.width * 0, y: view.frame.height * 0.2, width: 500, height: 160))
        whiteView.backgroundColor = UIColor.white
        whiteView.layer.shadowColor = UIColor.black.cgColor
        whiteView.layer.shadowOffset = CGSize.zero
        whiteView.layer.shadowOpacity = 0.5
        whiteView.layer.shadowRadius = 15
    
        
        
        // Button propterties
        button = UIButton(frame: CGRect(x: view.frame.width * 0.32, y: view.frame.height * 0.8, width: 125, height: 30))
        button.setBackgroundImage(#imageLiteral(resourceName: "broadSoftButton"), for: .normal)
        
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowOffset = CGSize.zero
        button.layer.shadowOpacity = 0.5
        button.layer.shadowRadius = 15
        
        
        button.setTitle("Login", for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
        
    
        
        // TextField properties
        idText = VibrantTextField(frame: CGRect(x: view.frame.width * 0.2, y: view.frame.height * 0.8, width: 200, height: 25))
        
        idText.delegate = self
        
        idText.placeholder = "User Id"
        idText.borderStyle = .none
        idText.borderStyle = UITextBorderStyle.none
        idText.backgroundColor = UIColor.clear
        
        let borderLine = UIView()
        let height = 1.5
        borderLine.frame = CGRect(x: 0, y: Double(idText.frame.height) - height, width: Double(idText.frame.width), height: height)
        
        borderLine.backgroundColor = UIColor.orange
        idText.addSubview(borderLine)
        
        passwordText = VibrantTextField(frame: CGRect(x: view.frame.width * 0.2, y: view.frame.height * 0.8, width: 200, height: 25))
        passwordText.placeholder = "Password"
        passwordText.borderStyle = .none
        passwordText.isSecureTextEntry = true
        passwordText.backgroundColor = UIColor.clear
        
        creditsText = UILabel(frame: CGRect(x: view.frame.width * 0.23, y: view.frame.height * 0.87, width: 200, height: 50))
        creditsText.textAlignment = .center
        creditsText.backgroundColor = UIColor.clear
        creditsText.layer.shadowColor = UIColor.black.cgColor
        creditsText.layer.shadowOffset = CGSize.zero
        creditsText.layer.shadowOpacity = 0.3
        creditsText.layer.shadowRadius = 15
        creditsText.text = "Broadsoft Inc"
        creditsText.font = UIFont (name: "HelveticaNeue-Bold", size: 12)
        
        
        creditsText2 = UILabel(frame: CGRect(x: view.frame.width * 0.23, y: view.frame.height * 0.90, width: 200, height: 50))
        creditsText2.textAlignment = .center
        creditsText2.backgroundColor = UIColor.clear
        creditsText2.layer.shadowColor = UIColor.black.cgColor
        creditsText2.layer.shadowOffset = CGSize.zero
        creditsText2.layer.shadowOpacity = 0.3
        creditsText2.layer.shadowRadius = 15
        creditsText2.text = "Software Company"
        creditsText2.font = UIFont (name: "HelveticaNeue-Bold", size: 12)
        
        
        creditsText3 = UILabel(frame: CGRect(x: view.frame.width * 0.23, y: view.frame.height * 0.93, width: 200, height: 50))
        creditsText3.textAlignment = .center
        creditsText3.backgroundColor = UIColor.clear
        creditsText3.layer.shadowColor = UIColor.black.cgColor
        creditsText3.layer.shadowOffset = CGSize.zero
        creditsText3.layer.shadowOpacity = 0.3
        creditsText3.layer.shadowRadius = 15
        creditsText3.text = "Founded 1998"
        creditsText3.font = UIFont (name: "HelveticaNeue-Bold", size: 12)
        
       
        
        // Animations
        UIView.animate(withDuration: 2.5, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0, options: [], animations: {
            self.idText.center.y = self.view.frame.height * 0.29
            self.passwordText.center.y = self.view.frame.height * 0.35
            self.button.center.y = self.view.frame.height * 0.45
        }) { (true) in
            let pulse = PulseAnimation(numberOfPulses: 3, radius: 60, position: self.button.center)
            pulse.animationDuration = 3.0
            pulse.backgroundColor = UIColor.green.cgColor
            
            self.view.layer.insertSublayer(pulse, below: self.button.layer)
        }
        
        
        
        let borderLine2 = UIView()
        let height2 = 1.5
        borderLine2.frame = CGRect(x: 0, y: Double(passwordText.frame.height) - height2, width: Double(passwordText.frame.width), height: height2)
        
        borderLine2.backgroundColor = UIColor.orange
        passwordText.addSubview(borderLine2)
        
      
    
        
        // Adding all the views to the ViewController
        view.backgroundColor = UIColor.yellow
        
         view.addSubview(backgroundImageview)
        
        view.addSubview(whiteView)
        view.addSubview(passwordText)
        view.addSubview(idText)
        view.addSubview(button)
        view.addSubview(imageView)
        view.addSubview(creditsText)
        view.addSubview(creditsText2)
        view.addSubview(creditsText3)
       
        // setting title of navigation bar
        self.title = "Login"
  
    }

    // Login button action function
    func buttonAction(sender: UIButton!) {

        let pulse = PulseAnimation(numberOfPulses: 1, radius: 110, position: button.center)
        pulse.animationDuration = 0.8
        pulse.backgroundColor = UIColor.green.cgColor
        
        self.view.layer.insertSublayer(pulse, below: button.layer)
        let vc = MessageThreadViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        idText.shake()
        passwordText.shake()
    }




}
